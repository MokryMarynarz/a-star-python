
from random import randrange
import pygame, sys
import constants

########################pyGame###################################

SCREEN_WIDTH = 480
SCREEN_HEIGHT = 480
NUMBER_OF_BLOCKS_WIDE = 20
NUMBER_OF_BLOCKS_HIGH = 20
BLOCK_HEIGHT = round(SCREEN_HEIGHT / NUMBER_OF_BLOCKS_HIGH)
BLOCK_WIDTH = round(SCREEN_WIDTH / NUMBER_OF_BLOCKS_WIDE)

def get_tile_color(tile_contents):
    tile_color = constants.GOLD
    if tile_contents == "#":
        tile_color = constants.DARKGREY
    if tile_contents == "0":
        tile_color = constants.GREEN
    if tile_contents == "^":
        tile_color = constants.BLACK
    if tile_contents == "%":
        tile_color = constants.RED
    return tile_color

def draw_map(surface, map_tiles):
    for j, tile in enumerate(map_tiles):
        for i, tile_contents in enumerate(tile):
            myrect = pygame.Rect(i*BLOCK_WIDTH, j*BLOCK_HEIGHT, BLOCK_WIDTH, BLOCK_HEIGHT)
            pygame.draw.rect(surface, get_tile_color(tile_contents), myrect)

def draw_grid(surface):
    for i in range(NUMBER_OF_BLOCKS_WIDE):
        new_height = round(i * BLOCK_HEIGHT)
        new_width = round(i * BLOCK_WIDTH)
        pygame.draw.line(surface, constants.BLACK, (0, new_height), (SCREEN_WIDTH, new_height), 2)
        pygame.draw.line(surface, constants.BLACK, (new_width, 0), (new_width, SCREEN_HEIGHT), 2)

def game_loop(surface, world_map):
    flag = True;
    while flag:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
            if event.type==pygame.KEYUP:
                flag=False
        draw_map(surface, world_map)
        draw_grid(surface)
        pygame.display.update()

def initialize_game():
    pygame.init()
    surface = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption(constants.TITLE)
    surface.fill(constants.UGLY_PINK)
    return surface

def read_map(word_map):
    world_map=[];
    for line in word_map.splitlines():
            world_map.append(line);

    print(world_map)
    return (world_map)

###################################aStar##########################################################

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Point:
    def get_x():
        return x
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Mesh:
    def __init__(self, size_x, size_y, walls, starting_point, finish_point):
        self.size_x = size_x
        self.size_y = size_y
        self.walls = walls
        self.starting_point = starting_point
        self.finish_point = finish_point
        self.matrix = [[0 for x in range(size_x)] for y in range(size_y)]
        for x in range(400): #gestosc rozmieszczonych scian
            if randrange(10)%2==0:
                self.matrix[randrange(size_x)][randrange(size_y)]=-1
        self.matrix[starting_point.x][starting_point.y] = 4
        self.matrix[finish_point.x][finish_point.y] = 5
        self.pyMap=[]

    def check_if_not_out(self, i, j):
        inBoundsX = (i >= 0) and (i < self.size_x);
        inBoundsY = (j >= 0) and (j < self.size_y);
        if(inBoundsX==True and inBoundsY==True):
            return True
        else:
            return False

    def print_map(self):
        eachLine=""
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                if self.matrix[i][j]==-1:
                    print(bcolors.OKGREEN+"#"+ bcolors.ENDC, end=" ")
                    eachLine=eachLine+"#";
                if self.matrix[i][j]==0:
                    print(bcolors.BOLD+"0"+ bcolors.ENDC, end=" ")
                    eachLine = eachLine + "0";
                if self.matrix[i][j]==4:
                    print(bcolors.HEADER+"^"+ bcolors.ENDC, end=" ")
                    eachLine = eachLine + "^";
                if self.matrix[i][j]==5:
                    print(bcolors.WARNING+"%"+ bcolors.ENDC, end=" ")
                    eachLine = eachLine + "%";
                if self.matrix[i][j]==7:
                    print(bcolors.OKBLUE+"x"+ bcolors.ENDC, end=" ")
                    eachLine = eachLine + "x";
            print()
            eachLine=eachLine+"\n"
        self.pyMap.append(eachLine)

    def mark_road_map(self, road):
        for x in range(len(road)):
            self.matrix[road[x].x][road[x].y]=7
        self.matrix[self.starting_point.x][self.starting_point.y]=4
        self.matrix[self.finish_point.x][self.finish_point.y]=5

class Astar:
    def __init__(self, size_x, size_y, walls, starting_point, finish_point):
        self.mesh = Mesh(size_x, size_y, walls, starting_point, finish_point)
        self.actual_position = self.mesh.starting_point
        self.closed_list = []
        self.open_list = []
        self.open_list.append(self.actual_position)

    def manhattan_distance(self, actual_position):
        return abs(actual_position.x - self.mesh.finish_point.x) + abs(actual_position.y - self.mesh.finish_point.y)

    def check_neighbours(self, actual_position):
        self.closed_list.append(actual_position)
        for x in range(len(self.open_list)):
            if actual_position.y==self.open_list[x].y and actual_position.x==self.open_list[x].x:
                index=x;
        self.open_list = self.open_list[:index] + self.open_list[index + 1:]
        for i in range(actual_position.x - 1, actual_position.x + 2):
            for j in range(actual_position.y - 1, actual_position.y + 2):
                flag = True
                if self.mesh.check_if_not_out(i, j) and self.mesh.matrix[i][j]!=-1:
                    for x in range(len(self.closed_list)):
                        if(self.closed_list[x].x==i and self.closed_list[x].y==j):
                            flag=False;
                    if flag==True:
                        temp_position=Point(i,j)
                        self.open_list.append(temp_position)

    def calc(self):
        newPoint = Point(0,0)
        lowest_cost=self.manhattan_distance(newPoint)
        self.mesh.matrix[self.actual_position.x][self.actual_position.y]=0
        for x in range(len(self.open_list)):
            if (self.manhattan_distance(self.open_list[x])<=lowest_cost):
                lowest_cost=self.manhattan_distance(self.open_list[x]);
                finalPoint=Point(self.open_list[x].x,self.open_list[x].y)
        self.actual_position=finalPoint
        self.mesh.matrix[finalPoint.x][finalPoint.y]=4

    def main_loop(self):
            while self.actual_position.y!=self.mesh.finish_point.y or self.actual_position.x!=self.mesh.finish_point.x:
                self.check_neighbours(self.actual_position)
                self.calc()
                self.mesh.print_map()
                print()
            self.mesh.mark_road_map(self.closed_list)
            self.mesh.print_map()
            for i in self.mesh.pyMap:
                world_map = read_map(i)
                surface = initialize_game()
                game_loop(surface, world_map)

astar = Astar(NUMBER_OF_BLOCKS_WIDE, NUMBER_OF_BLOCKS_HIGH, 0, Point(1,1), Point(NUMBER_OF_BLOCKS_WIDE-1, NUMBER_OF_BLOCKS_HIGH-1))
astar.main_loop();